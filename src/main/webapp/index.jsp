<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
   <html>
  <head>
    <title>Kalkulator kredytowy</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="style.css" type="text/css" />
  </head>
  <body>

      <h3>KALKULATOR KREDYTOWY</h3>

      <FORM action="table" METHOD="POST">
        <TABLE>
        <TR>
          <TD>Kwota kredytu (PLN):</TD>
          <TD><INPUT type="text" name="kwota" value=""></TD>
        </TR>
        <TR>
          <TD>Okres kredytowania (w latach):</TD>
          <TD><INPUT type="text" name="okres" value=""></TD>
        </TR>
        <TR>
          <TD>Oprocentowanie (%):</TD>
          <TD><INPUT type="text" name="procent" value=""></TD>
        </TR>
        <TR>
          <TD>Rodzaj rat:</TD>
          <TD><input type="radio" name="rodzaj"  value="rowna" checked/> równe </TD>
        </TR>
        <TR>
          <TD>&nbsp;</TD>
          <TD><input type="radio" name="rodzaj"  value="malejaca"/> malejące </TD>
        </TR>
        <TR>
          <TD class='t'><BR><INPUT type="submit" name="submit" value="Oblicz"></TD>
          <TD><BR><INPUT type="submit" name="generate" value="Wygeneruj PDF"></TD>
        </TR>
        </TABLE>
      </FORM>
      
      <% if (request.getParameter("submit") != null) out.println("Jest!");%>
      

    </body>
</html>