package creditcalc;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

import web.TableServlet;

public class TestTableServlet extends Mockito {
	
	@Test(expected = NullPointerException.class) 
	public void servlet_should_not_show_table_if_kwota__is_null() throws IOException, ServletException{
		
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		TableServlet tableServlet = new TableServlet();
		
		when(request.getParameter("kwota")).thenReturn(null);
		when(request.getParameter("okres")).thenReturn("1");
		when(request.getParameter("procent")).thenReturn("1");
		
		tableServlet.doPost(request, response);
		
		
	}
	
	@Test(expected = NullPointerException.class) 
	public void servlet_should_not_show_table_if_okres_is_null() throws IOException, ServletException{
		
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		TableServlet tableServlet = new TableServlet();
		
		when(request.getParameter("kwota")).thenReturn("500");
		when(request.getParameter("okres")).thenReturn(null);
		when(request.getParameter("procent")).thenReturn("1");
		
		tableServlet.doPost(request, response);
		
		
	}
	
	@Test(expected = NullPointerException.class) 
	public void servlet_should_not_show_table_if_procent_is_null() throws IOException, ServletException{
		
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		TableServlet tableServlet = new TableServlet();
		
		when(request.getParameter("kwota")).thenReturn("500");
		when(request.getParameter("okres")).thenReturn("1");
		when(request.getParameter("procent")).thenReturn(null);
		
		tableServlet.doPost(request, response);
		
		
	}
}
