package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.PDFcreator;
import services.Table;
import model.Credit;


@WebServlet("/table")
public class TableServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private int kwota;
	private int okres;
	private float procent;
       

	public void doPost(HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		
		
		try {
		
			kwota = Integer.parseInt(request.getParameter("kwota"));
			okres = Integer.parseInt(request.getParameter("okres"));
			procent = Float.valueOf(request.getParameter("procent").replace(",", "."));
		}
		
		catch (Exception e) {
			
			response.sendRedirect("/");
		}
		
		if ((kwota < 500 || kwota > 100000000) || (okres < 1 || okres > 50) || (procent < 0 || procent > 100))
			
			response.sendRedirect("/");
				
		Credit credit = new Credit();
		credit.setKwota(kwota);
		credit.setOkres(okres);
		credit.setProcent(procent);
		
		Table table = new Table(credit);
		
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		StringBuilder stringBuilder = new StringBuilder();
		
		stringBuilder.append(""
				+ "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" "
				+ "\"http://www.w3.org/TR/html4/loose.dtd\">"
				+ "<html>"
				+ "<head>"
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
				+ "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />"
				+ "<title>Kalkulator kredytowy</title>"
				+ "</head>"
				+ "<body>"
				+ "<br/><br/>"
				+ "<table class='t'>"
				+ "<tr>"
				+ "<th>Miesiąc</th>"
				+ "<th>Saldo</th>"
				+ "<th>Część odsetkowa</th>"
				+ "<th>Część kapitałowa</th>"
				+ "<th>Rata</th>"
				+ "<th>Pozostało</th>"
				+ "</tr>"
				+ ( request.getParameter("rodzaj").equals("rowna") ?
						table.makeEqualsInstallment() : table.makeDegreasingInstallment()) 
				+ "</table>"
				+ "</body>"
				+ "</html>");
		
		Reader reader = new StringReader(stringBuilder.toString());
		
		if (request.getParameter("generate") != null){
		       new PDFcreator(request, response, reader);
		}
		else {
		PrintWriter out = response.getWriter();	
			out.println(stringBuilder);
		out.close();
		}
	}
	


}
