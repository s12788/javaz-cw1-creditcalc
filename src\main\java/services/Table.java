package services;

import model.Credit;

public class Table {
	
	private int kwota;
	private int okres;
	private double procent;
	
    private double saldo;
    private double czesc_kapitalowa;
    private double czesc_odsetkowa;
    private double pozostalo;
    private int n;
    
	private StringBuilder stringBuilder = new StringBuilder();

	private double rata;
	
	private String headers = "<tr>"
			+ "<th>Miesiąc</th>"
			+ "<th>Saldo</th>"
			+ "<th>Część odsetkowa</th>"
			+ "<th>Część kapitałowa</th>"
			+ "<th>Rata</th>"
			+ "<th>Pozostało</th>"
			+ "</tr>";
	
	public Table(Credit credit) {
		
		this.kwota = credit.getKwota();
		this.okres = credit.getOkres()*12;
		this.procent = credit.getProcent()/100/12;
		
		rata=kwota*Math.pow(1+procent, okres)
				*procent/(Math.pow(1+procent, okres)-1);

//		rata = Math.round(rata*100d)/100d;
	}
	
	public StringBuilder makeEqualsInstallment (){
		
		saldo=kwota;

	    for (n=1;n<=okres;n++){
	  
	    czesc_kapitalowa=kwota*procent*Math.pow(1+procent,n-1)
	    /(Math.pow(1+procent, okres)-1);
	    czesc_odsetkowa=rata-czesc_kapitalowa;
	   
	    pozostalo=saldo-czesc_kapitalowa;

	    if (rata>saldo) {
	        rata=saldo;
	        czesc_kapitalowa=rata-czesc_odsetkowa;
	    }
	    
	    
	    stringBuilder.append("<tr><td>"+n+"</td><td>"+roundT2dP(saldo)+
	    		"</td><td>"+roundT2dP(czesc_odsetkowa)+
	    		"</td><td>"+roundT2dP(czesc_kapitalowa)+
	    		"</td><td>"+roundT2dP(rata)+
	    		"</td><td>"+roundT2dP(pozostalo)+"</td></tr>");


	    saldo=pozostalo;
	    
	    if(n%12==0 && n!=okres){
	    	
	    	stringBuilder.append(headers);
	    	
	    }
	    
	    }
		
		
		return stringBuilder;
	}
	
	public StringBuilder makeDegreasingInstallment (){
		
		
	    saldo=kwota;

	    czesc_kapitalowa=(double)kwota/okres;
	    czesc_odsetkowa=kwota*procent;
	    rata=czesc_kapitalowa+czesc_odsetkowa;
	    pozostalo=saldo-czesc_kapitalowa;

	    stringBuilder.append("<tr><td>"+1+"</td><td>"+roundT2dP(saldo)+
	    		"</td><td>"+roundT2dP(czesc_odsetkowa)+
	    		"</td><td>"+roundT2dP(czesc_kapitalowa)+
	    		"</td><td>"+roundT2dP(rata)+
	    		"</td><td>"+roundT2dP(pozostalo)+"</td></tr>");
	    

	    saldo=pozostalo;


	    for (n=2;n<=okres;n++){
	  
	    czesc_odsetkowa=pozostalo*procent;

	    pozostalo=saldo-czesc_kapitalowa;

	            if (rata>=saldo) {
	        czesc_kapitalowa=saldo-czesc_odsetkowa;
	    }

	    rata=czesc_kapitalowa+czesc_odsetkowa;
	    
	    stringBuilder.append("<tr><td>"+n+"</td><td>"+roundT2dP(saldo)+
	    		"</td><td>"+roundT2dP(czesc_odsetkowa)+
	    		"</td><td>"+roundT2dP(czesc_kapitalowa)+
	    		"</td><td>"+roundT2dP(rata)+
	    		"</td><td>"+roundT2dP(pozostalo)+"</td></tr>");



	    saldo=pozostalo;
	    
	    if(n%12==0 && n!=okres){
	    	
	    	stringBuilder.append(headers);
	    	
	    }
	    
	    }
		
		return stringBuilder;
	}
	
	
	private String roundT2dP (double value){     //Round a double to 2 decimal places 
		
		value = Math.round(value*100d)/100d;
		String money = String.format("%.2f", value);
		return money;
	}

}
