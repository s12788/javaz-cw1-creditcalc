package model;

public class Credit {
	
	private int kwota;
	private int okres;
	private float procent;
	
	public int getKwota() {
		return kwota;
	}
	public void setKwota(int kwota) {
		this.kwota = kwota;
	}
	public int getOkres() {
		return okres;
	}
	public void setOkres(int okres) {
		this.okres = okres;
	}
	public float getProcent() {
		return procent;
	}
	public void setProcent(float procent) {
		this.procent = procent;
	}
	
	

}
